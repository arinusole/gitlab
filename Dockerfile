# This file is a template, and might need editing before it works on your project.
FROM python:3.8-alpine

# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
# RUN apk --no-cache add postgresql-client

WORKDIR /python_api

COPY python-api.py /python_api

# COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir flask flask-jsonpify flask-restful

EXPOSE 5290
CMD [ "python", "/python_api/python-api.py" ]
